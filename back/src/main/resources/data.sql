INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');

/*lozinke korisnika su iste kao korisnicka imena*/

INSERT INTO drzava (naziv, oznaka) VALUES ( 'Srbija', 'SRB');
INSERT INTO drzava (naziv, oznaka) VALUES ( 'Amerika', 'USA');
INSERT INTO drzava (naziv, oznaka) VALUES ( 'Rusija', 'RUS');


INSERT INTO takmicar (ime_prezime, broj_medalja, datum_rodjenja,drzava_id) VALUES ( 'Marko Markovic', 12, '2000-12-01',1);
INSERT INTO takmicar (ime_prezime, broj_medalja, datum_rodjenja,drzava_id) VALUES ( 'Marko Milic', 22, '2000-11-01',2);
INSERT INTO takmicar (ime_prezime, broj_medalja, datum_rodjenja,drzava_id) VALUES ( 'Marko Peric', 10, '2000-10-01',3);

INSERT INTO prijava (datum_prijave, disciplina, takmicar_id) VALUES ( '2020-12-01', 'Troskok', 1);
INSERT INTO prijava (datum_prijave, disciplina, takmicar_id) VALUES ( '2020-11-01', 'Skok sa motkom', 2);
INSERT INTO prijava (datum_prijave, disciplina, takmicar_id) VALUES ( '2020-10-01', '100m', 3);




