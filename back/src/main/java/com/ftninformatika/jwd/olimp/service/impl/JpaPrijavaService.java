package com.ftninformatika.jwd.olimp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.olimp.model.Prijava;
import com.ftninformatika.jwd.olimp.repository.PrijavaRepository;
import com.ftninformatika.jwd.olimp.service.PrijavaService;

@Service
public class JpaPrijavaService implements PrijavaService{

	@Autowired
	private PrijavaRepository rep;
	
	@Override
	public Prijava findOneById(Long id) {
		// TODO Auto-generated method stub
		return rep.findOneById(id);
	}

	@Override
	public List<Prijava> findAll() {
		// TODO Auto-generated method stub
		return rep.findAll();
	}

	@Override
	public Prijava save(Prijava p) {
		// TODO Auto-generated method stub
		return rep.save(p);
	}

}
