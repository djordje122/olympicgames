package com.ftninformatika.jwd.olimp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.olimp.model.Drzava;
import com.ftninformatika.jwd.olimp.model.Takmicar;
import com.ftninformatika.jwd.olimp.repository.DrzavaRepository;
import com.ftninformatika.jwd.olimp.repository.TakmicarRepository;
import com.ftninformatika.jwd.olimp.service.DrzavaService;

@Service
public class JpaDrzavaService implements DrzavaService {

	@Autowired
	private TakmicarRepository takmicarRepository;
	
	@Autowired
	private DrzavaRepository rep;
	
	@Override
	public Drzava findOneById(Long id) {
		// TODO Auto-generated method stub
		return rep.findOneById(id);
	}

	@Override
	public List<Drzava> findAll() {
		// TODO Auto-generated method stub
		return rep.findAll();
	}

	@Override
	public Integer getBrojMedalja(Long id) {
		int brojMedalja=0;
		List<Takmicar> takmicari=takmicarRepository.findAll();
		for(Takmicar takmicar:takmicari) {
			if(takmicar.getDrzava().getId()==id) {
				brojMedalja=brojMedalja+takmicar.getBrojMedalja();
			}
		}
		return brojMedalja;
		
	}
	
}


