package com.ftninformatika.jwd.olimp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.olimp.web.dto.DrzavaDto;
import com.ftninformatika.jwd.olimp.model.Drzava;
import com.ftninformatika.jwd.olimp.service.DrzavaService;
import com.ftninformatika.jwd.olimp.support.DrzavaToDto;

@RestController
@RequestMapping(value = "/api/drzave",produces = MediaType.APPLICATION_JSON_VALUE)
public class DrzavaController {

	@Autowired
	private DrzavaService service;
	
	@Autowired
	private DrzavaToDto toDto;
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<DrzavaDto>> getAll() {
		List<Drzava> list=service.findAll();
		return new ResponseEntity<>(toDto.convert(list),HttpStatus.OK);
	}
	
		@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
		@GetMapping("/{id}")
		public ResponseEntity<DrzavaDto> getOne(@PathVariable Long id) {
			Drzava prijava = service.findOneById(id); 

			if (prijava != null) {
				return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
}
