package com.ftninformatika.jwd.olimp.service;

import java.util.List;

import com.ftninformatika.jwd.olimp.model.Drzava;

public interface DrzavaService {

	Drzava findOneById(Long id);
	
	List<Drzava> findAll();
	
	Integer getBrojMedalja(Long id) ;
}
