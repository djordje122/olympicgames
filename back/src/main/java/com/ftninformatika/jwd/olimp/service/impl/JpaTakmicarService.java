package com.ftninformatika.jwd.olimp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.olimp.model.Takmicar;
import com.ftninformatika.jwd.olimp.repository.TakmicarRepository;
import com.ftninformatika.jwd.olimp.service.TakmicarService;

@Service
public class JpaTakmicarService implements TakmicarService{

	@Autowired
	private TakmicarRepository rep;
	
	@Override
	public Takmicar findOneById(Long id) {
		// TODO Auto-generated method stub
		return rep.findOneById(id);
	}

	@Override
	public List<Takmicar> findAll() {
		// TODO Auto-generated method stub
		return rep.findAll();
	}

	@Override
	public Takmicar save(Takmicar t) {
		// TODO Auto-generated method stub
		return rep.save(t);
	}

	@Override
	public Takmicar update(Takmicar t) {
		// TODO Auto-generated method stub
		return rep.save(t);
	}

	public Takmicar delete(Long id) {
		Optional<Takmicar> takmicenje=Optional.of(findOneById(id));
		if(takmicenje.isPresent()) {
			rep.deleteById(id);
			return takmicenje.get();
		}
		return null;
	}

	@Override
	public Page<Takmicar> find(Long drzavaId, Integer medaljeOd, Integer medaljeDo, Integer pageNo) {
		
		if (medaljeOd == null) {
			medaljeOd = 0;
		}
		if (medaljeDo == null) {
			medaljeDo = Integer.MAX_VALUE;
		}
		if (drzavaId == null) {
			return rep.findByBrojMedaljaBetween(medaljeOd, medaljeDo, PageRequest.of(pageNo, 2));
		}
		
		return rep.findByDrzavaIdAndBrojMedaljaBetween(drzavaId, medaljeOd, medaljeDo, PageRequest.of(pageNo, 2));
	}

}
