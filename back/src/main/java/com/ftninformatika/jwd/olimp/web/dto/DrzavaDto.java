package com.ftninformatika.jwd.olimp.web.dto;

public class DrzavaDto {

	private Long id;
	private String naziv;
	private String oznaka;
	private Integer brojMedalja;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOznaka() {
		return oznaka;
	}
	public void setOznaka(String oznaka) {
		this.oznaka = oznaka;
	}
	public Integer getBrojMedalja() {
		return brojMedalja;
	}
	public void setBrojMedalja(Integer brojMedalja) {
		this.brojMedalja = brojMedalja;
	}
	
}
