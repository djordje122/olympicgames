package com.ftninformatika.jwd.olimp.support;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.olimp.web.dto.PrijavaDto;
import com.ftninformatika.jwd.olimp.model.Prijava;
import com.ftninformatika.jwd.olimp.service.PrijavaService;
import com.ftninformatika.jwd.olimp.service.TakmicarService;

@Component
public class DtoToPrijava implements Converter<PrijavaDto, Prijava>{

	@Autowired
	private PrijavaService service;
	
	@Autowired
	private TakmicarService takmService;
	
	@Override
	public Prijava convert(PrijavaDto dto) {
		Prijava entity;
		if(dto.getId()==null) {
			entity=new Prijava();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		
		if(entity!=null) {
			entity.setDatumPrijave(LocalDate.now());
			entity.setDisciplina(dto.getDisciplina());
			
			if(dto.getTakmicarId()!=null && takmService.findOneById(dto.getTakmicarId())!=null) {
				entity.setTakmicar(takmService.findOneById(dto.getTakmicarId()));
			}

		}
		
		return entity;
	}

}
