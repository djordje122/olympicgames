package com.ftninformatika.jwd.olimp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.olimp.web.dto.PrijavaDto;
import com.ftninformatika.jwd.olimp.model.Prijava;

@Component
public class PrijavaToDto implements Converter<Prijava, PrijavaDto>{

	@Override
	public PrijavaDto convert(Prijava source) {
		PrijavaDto dto=new PrijavaDto();
		dto.setId(source.getId());
		dto.setDatumPrijave(source.getDatumPrijave().toString());
		dto.setDisciplina(source.getDisciplina());
		dto.setTakmicarId(source.getTakmicar().getId());
		dto.setTakmicarImePrezime(source.getTakmicar().getImePrezime());
		return dto;
	}
	
	public List <PrijavaDto> convert(List <Prijava> list) {
		List <PrijavaDto> dto=new ArrayList<>();
		for(Prijava takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}
	

}
