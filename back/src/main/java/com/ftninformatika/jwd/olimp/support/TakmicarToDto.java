package com.ftninformatika.jwd.olimp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.olimp.web.dto.TakmicarDto;
import com.ftninformatika.jwd.olimp.model.Takmicar;

@Component
public class TakmicarToDto implements Converter<Takmicar, TakmicarDto>{

	@Override
	public TakmicarDto convert(Takmicar source) {
		TakmicarDto dto=new TakmicarDto();
		dto.setId(source.getId());
		dto.setBrojMedalja(source.getBrojMedalja());
		dto.setId(source.getId());
		dto.setImePrezime(source.getImePrezime());
		dto.setDatumRodjenja(source.getDatumRodjenja().toString());
		dto.setDrzavaId(source.getDrzava().getId());
		dto.setDrzavaNaziv(source.getDrzava().getNaziv());
		dto.setDrzavaOznaka(source.getDrzava().getOznaka());
		return dto;
	}
	
	public List <TakmicarDto> convert(List <Takmicar> list) {
		List <TakmicarDto> dto=new ArrayList<>();
		for(Takmicar takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}

}
