package com.ftninformatika.jwd.olimp.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.olimp.web.dto.TakmicarDto;
import com.ftninformatika.jwd.olimp.model.Takmicar;
import com.ftninformatika.jwd.olimp.service.TakmicarService;
import com.ftninformatika.jwd.olimp.support.DtoToTakmicar;
import com.ftninformatika.jwd.olimp.support.TakmicarToDto;

@RestController
@RequestMapping(value = "/api/takmicari",produces = MediaType.APPLICATION_JSON_VALUE)
public class TakmicarController {
	
	@Autowired
	private TakmicarService service;
	
	@Autowired
	private TakmicarToDto toDto;
	
	@Autowired
	private DtoToTakmicar toEntity;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TakmicarDto>> getAll(
			@RequestParam(required=false) Long drzavaId, 
			@RequestParam(required=false) Integer medaljeOd,
			@RequestParam(required=false) Integer medaljeDo,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/vina?pageNo=0 ili neki drugi broj umesto 0

		Page<Takmicar> page = service.find(drzavaId, medaljeOd,medaljeDo, pageNo) ;

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<TakmicarDto> getOne(@PathVariable Long id) {
		Takmicar prijava = service.findOneById(id); 

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicarDto> create(@Valid @RequestBody TakmicarDto vinoDto) { 
		Takmicar vino = toEntity.convert(vinoDto);

		Takmicar sacuvaniTakmicar = service.save(vino);

		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicarDto> update(@PathVariable Long id, @Valid @RequestBody TakmicarDto vinoDto) { 

		if (!id.equals(vinoDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		// ukoliko ne postoji vino
		if (service.findOneById(id) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		Takmicar vino = toEntity.convert(vinoDto); 

		Takmicar sacuvanoVino = service.update(vino);

		return new ResponseEntity<>(toDto.convert(sacuvanoVino), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Takmicar obrisaniTakmicar = service.findOneById(id);

		if (obrisaniTakmicar != null) {
			service.delete(obrisaniTakmicar.getId());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}
