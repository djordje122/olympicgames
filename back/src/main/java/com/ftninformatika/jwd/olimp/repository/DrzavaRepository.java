package com.ftninformatika.jwd.olimp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.olimp.model.Drzava;

@Repository
public interface DrzavaRepository extends JpaRepository<Drzava, Long>{
	
	Drzava findOneById(Long id);
}
