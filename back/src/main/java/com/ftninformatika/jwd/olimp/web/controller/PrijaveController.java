package com.ftninformatika.jwd.olimp.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.olimp.web.dto.PrijavaDto;
import com.ftninformatika.jwd.olimp.model.Prijava;
import com.ftninformatika.jwd.olimp.service.PrijavaService;
import com.ftninformatika.jwd.olimp.support.DtoToPrijava;
import com.ftninformatika.jwd.olimp.support.PrijavaToDto;

@RestController
@RequestMapping(value = "/api/prijave",produces = MediaType.APPLICATION_JSON_VALUE)
public class PrijaveController {
	
	@Autowired
	private PrijavaService service;
	
	@Autowired
	private DtoToPrijava toEntity;
	
	@Autowired
	private PrijavaToDto toDto;
	
	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrijavaDto> create(@Valid @RequestBody PrijavaDto vinoDto) { 
		Prijava vino = toEntity.convert(vinoDto);

		Prijava sacuvaniTakmicar = service.save(vino);

		return new ResponseEntity<>(toDto.convert(sacuvaniTakmicar), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
	@GetMapping
	public ResponseEntity<List<PrijavaDto>> getAll() {
		List<Prijava> list=service.findAll();
		return new ResponseEntity<>(toDto.convert(list),HttpStatus.OK);
	}
	
		@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
		@GetMapping("/{id}")
		public ResponseEntity<PrijavaDto> getOne(@PathVariable Long id) {
			Prijava prijava = service.findOneById(id); 

			if (prijava != null) {
				return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}

}
