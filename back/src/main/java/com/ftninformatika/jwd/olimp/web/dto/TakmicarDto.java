package com.ftninformatika.jwd.olimp.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class TakmicarDto {
	
	private Long id;
	
	@Size(max = 60)
	private String imePrezime;
	@Positive
	private Integer brojMedalja;
	
	private String datumRodjenja;
	
	private Long drzavaId;
	private String drzavaNaziv;
	private String drzavaOznaka;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getImePrezime() {
		return imePrezime;
	}
	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}
	public Integer getBrojMedalja() {
		return brojMedalja;
	}
	public void setBrojMedalja(Integer brojMedalja) {
		this.brojMedalja = brojMedalja;
	}
	public String getDatumRodjenja() {
		return datumRodjenja;
	}
	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}
	public Long getDrzavaId() {
		return drzavaId;
	}
	public void setDrzavaId(Long drzavaId) {
		this.drzavaId = drzavaId;
	}
	public String getDrzavaNaziv() {
		return drzavaNaziv;
	}
	public void setDrzavaNaziv(String drzavaNaziv) {
		this.drzavaNaziv = drzavaNaziv;
	}
	public String getDrzavaOznaka() {
		return drzavaOznaka;
	}
	public void setDrzavaOznaka(String drzavaOznaka) {
		this.drzavaOznaka = drzavaOznaka;
	}
	
	
}
