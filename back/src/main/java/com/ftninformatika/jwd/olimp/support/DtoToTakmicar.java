package com.ftninformatika.jwd.olimp.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.olimp.web.dto.TakmicarDto;
import com.ftninformatika.jwd.olimp.model.Takmicar;
import com.ftninformatika.jwd.olimp.service.DrzavaService;
import com.ftninformatika.jwd.olimp.service.TakmicarService;

@Component
public class DtoToTakmicar implements Converter<TakmicarDto, Takmicar> {

	@Autowired
	private TakmicarService service;
	
	@Autowired
	private DrzavaService drzService;
	
	@Override
	public Takmicar convert(TakmicarDto dto) {
		Takmicar entity;
		if(dto.getId()==null) {
			entity=new Takmicar();
		}else {
			entity=service.findOneById(dto.getId());
		}
		
		
		if(entity!=null) {
			entity.setDatumRodjenja(getLocalDate(dto.getDatumRodjenja()));
			entity.setBrojMedalja(dto.getBrojMedalja());
			entity.setImePrezime(dto.getImePrezime());
			
			
			if(dto.getDrzavaId()!=null && drzService.findOneById(dto.getDrzavaId())!=null) {
				entity.setDrzava(drzService.findOneById(dto.getDrzavaId()));
			}
			
			

		}
		
		return entity;
	}
	
	 private LocalDate getLocalDate(String dateTime) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        return LocalDate.parse(dateTime, formatter);
	    }

}
