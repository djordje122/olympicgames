package com.ftninformatika.jwd.olimp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.olimp.web.dto.DrzavaDto;
import com.ftninformatika.jwd.olimp.model.Drzava;
import com.ftninformatika.jwd.olimp.service.DrzavaService;

@Component
public class DrzavaToDto implements Converter<Drzava, DrzavaDto> {
	
	@Autowired
	private DrzavaService drzavaService;

	@Override
	public DrzavaDto convert(Drzava source) {
		DrzavaDto dto=new DrzavaDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setOznaka(source.getOznaka());
		dto.setBrojMedalja(drzavaService.getBrojMedalja(source.getId()));
		System.out.println(source.getOznaka());
		return dto;
	}
	
	public List <DrzavaDto> convert(List <Drzava> list) {
		List <DrzavaDto> dto=new ArrayList<>();
		for(Drzava takmicenje:list) {
			dto.add(convert(takmicenje));
		}
		return dto;
		
	}

}
