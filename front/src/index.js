import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes,
  Navigate,
} from "react-router-dom";
import { Navbar, Nav, Button, Container } from "react-bootstrap";
import Takmicari from "./components/Olimp/Takmicari";
import AddTakmicar from "./components/Olimp/AddTakmicar";
import AddPrijava from "./components/Olimp/AddPrijava";
import { logout } from "./services/auth";
import Login from "./components/Login/Login";
import Statistika from "./components/Olimp/Statistika";

class App extends React.Component {
  render() {
    return (
      <>
        <Router>
          <Navbar expand bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/">
              JWD
            </Navbar.Brand>
            {window.localStorage["role"] == "ROLE_ADMIN" ||
            window.localStorage["role"] == "ROLE_KORISNIK" ? (
              <Nav>
                <Nav.Link as={Link} to="/takmicari">
                  Takmicar
                </Nav.Link>
              </Nav>
            ) : null}

            {window.localStorage["jwt"] ? (
              <Button onClick={() => logout()}>Log out</Button>
            ) : (
              <Nav.Link as={Link} to="/login">
                Log in
              </Nav.Link>
            )}
          </Navbar>
          <Container style={{ paddingTop: "10px" }}>
            <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="/takmicari" element={<Takmicari />} />
              <Route path="/takmicari/add" element={<AddTakmicar />} />
              <Route path="/takmicari/statistika" element={<Statistika />} />
              <Route
                path="/takmicari/prijava/:id"
                element={<AddPrijava />}
              />
              <Route path="/login" element={<Login />} />
            </Routes>
          </Container>
        </Router>
      </>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
