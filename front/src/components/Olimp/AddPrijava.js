import React from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withParams, withNavigation } from "../../routeconf";

class AddPrijava extends React.Component {
  constructor(props) {
    super(props);

    const sportskaDisciplina = { takmicarId: this.props.params.id, disciplina: "" };

    this.state = {
      sportskaDisciplina: sportskaDisciplina,
    };
  }

  componentDidMount() {
    console.log("PROPS" + this.state.takmicarId);
  }

  createPrijava(e) {
    let sportskaDisciplina = this.state.sportskaDisciplina;
    let dto = {
      takmicarId: sportskaDisciplina.takmicarId,
      disciplina: sportskaDisciplina.disciplina,
    };

    try {
      OGAxios.post("/prijave/", dto).then((res) => {
        console.log(res);
        this.props.navigate("/takmicari");
      });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <div>
        <Form.Group>
          <Form.Label>Disciplina</Form.Label>
          <Form.Control
            name="disciplina"
            as="input"
            type="text"
            onChange={(e) => this.onInputChange(e)}
          ></Form.Control>
        </Form.Group>
        <Button onClick={() => this.createPrijava()}>Dodaj</Button>
      </div>
    );
  }

  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let sportskaDisciplina = this.state.sportskaDisciplina;
    sportskaDisciplina[name] = value;
    console.log(sportskaDisciplina);
    this.setState({ sportskaDisciplina });
  }
}

export default withNavigation(withParams(AddPrijava));
