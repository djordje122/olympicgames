import React from "react";
import { Row, Col, Button, Form,Table } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation, withParams } from "../../routeconf";

class Statistika extends React.Component {

    constructor(props) {
        super(props);

    this.state = {
        drzave: []
      };
    }

    componentDidMount() {
        this.getDrzave();
      }

      getDrzave() {
        OGAxios.get("/drzave")
          .then((res) => {
            console.log(res);
            this.setState({ drzave: res.data });
          })
          .catch((err) => {
            console.log(err);
          });
      }

  


      renderDrzave() {
        let sortirano=this.state.drzave.sort((a,b) => {
            return a.brojMedalja - b.brojMedalja;
        });
        return this.state.drzave.map((drz) => {
          return (
            <tr key={drz.id}>
              <td>{drz.naziv}</td>
              <td>{drz.brojMedalja}</td>
              <td>
                {" "}
               
              </td>
            </tr>
          );
        });
      }

    render() {
        return (
          <>
                <br />
                <div className="displayCompetitorTable">

                  <Table>
                    <thead>
                      <tr>
                        <th>Drzava</th>
                        <th>Broj osvojenih medalja</th>
                        
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>{this.renderDrzave()}</tbody>
                  </Table>
                 
                  <br />
                </div>
          </>
        );
      }
}

export default withNavigation(withParams(Statistika));