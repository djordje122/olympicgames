import React from "react";
import { Row, Col, Button, Table, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation, withParams } from "../../routeconf";

class Takmicari extends React.Component {
  constructor(props) {
    super(props);

    let search = {
      drzavaId: "",
      medaljeOd: "",
      medaljeDo: "",
      totalPages: "",
    };

    this.state = {
      takmicari: [],
      search: search,
      drzave: [],
      pageNo: 0,
      totalPages: 2,
      check: true,
    };
  }

  componentDidMount() {
    this.getTakmicari(0);
    this.getDrzave();
  }

  getTakmicari(newPageNo) {
    if (newPageNo < 0) {
      newPageNo = 0;
    }

    console.log(newPageNo);

    let search = this.state.search;
    let config = {
      params: {
        drzavaId: search.drzavaId,
        medaljeOd: search.medaljeOd,
        medaljeDo: search.medaljeDo,
        pageNo: newPageNo,
      },
    };

    OGAxios.get("/takmicari", config)

      .then((res) => {
        this.setState({ pageNo: newPageNo });
        console.log(res);
        this.setState({
          takmicari: res.data,
          totalPages: res.headers["total-pages"],
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getDrzave() {
    OGAxios.get("/drzave")
      .then((res) => {
        console.log(res);
        this.setState({ drzave: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onSearchInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let search = this.state.search;
    search[name] = value;
    console.log(search);
    this.setState({ search: search});
    this.getTakmicari(0);
  }

  goToAddTakmicar() {
    this.props.navigate("/takmicari/add");
  }

  goToStatistika() {
    this.props.navigate("/takmicari/statistika");
  }

  toPrijava(id) {
    this.props.navigate("/takmicari/prijava/" + id);
  }

  deleteTakmicar(id) {
    OGAxios.delete("/takmicari/" + id)
      .then((res) => {
        console.log(res);
        alert("Uspesno obrisan takmicar")
        window.location.reload();

      })
      .catch((err) => {
        console.log(err);
        window.location.reload(); 
      });
  }

  renderDrzaveInDropDown() {
    return this.state.drzave.map((drzava) => {
      return (
        <option value={drzava.id} key={drzava.id}>
          {drzava.naziv}
        </option>
      );
    });
  }

  renderTakmicari() {
    return this.state.takmicari.map((takm) => {
      return (
        <tr key={takm.id}>
          <td>{takm.imePrezime}</td>
          <td>{takm.drzavaNaziv}</td>
          <td>{takm.datumRodjenja}</td>
          <td>{takm.brojMedalja}</td>
          {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.deleteTakmicar(takm.id)}>Obrisi</Button></td>
                        : <td></td>}
          <td>
            {" "}
            {window.localStorage["role"] == "ROLE_KORISNIK" ? (
              <Button  onClick={() => this.toPrijava(takm.id)}>
                Prijava
              </Button>
            ) : null}
          </td>
        </tr>
      );
    });
  }
  

  render() {
    return (
      <>
        {window.localStorage["role"] == "ROLE_ADMIN" ||
        window.localStorage["role"] == "ROLE_KORISNIK" ? (
          <div>
            <div className="search">
              <Form style={{ width: "100%" }}>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>Drzava</Form.Label>
                      <Form.Select
                        name="drzavaId"
                        as="input"
                        type="number"
                        onChange={(e) => this.onSearchInputChange(e)}
                      >
                        <option value=""></option>
                        {this.renderDrzaveInDropDown()}
                      </Form.Select>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col>
                      <Form.Label>Broj Medalja Od</Form.Label>
                      <Form.Control
                        name="medaljeOd"
                        as="input"
                        type="number"
                        min="0"
                        step="1"
                        onChange={(e) => this.onSearchInputChange(e)}
                      ></Form.Control>
                       <Form.Label>Broj Medalja Do</Form.Label>
                      <Form.Control
                        name="medaljeDo"
                        as="input"
                        type="number"
                        min="0"
                        step="1"
                        onChange={(e) => this.onSearchInputChange(e)}
                      ></Form.Control>
                  </Col>
                </Row>
              </Form>
              <Row>
                <Col>
                  {/* <Button onClick={() => this.getTakmicari(0)}>
                    Pretrazi
                  </Button> */}
                </Col>
              </Row>
            </div>
            <br />
            <div className="displayCompetitorTable">
              {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <Button onClick={() => this.goToAddTakmicar()}>
                  Dodaj Takmicara
                </Button>
              ) : null}
                {window.localStorage["role"] == "ROLE_ADMIN" ? (
                <Button variant="warning" onClick={() => this.goToStatistika()}>
                  Statistika
                </Button>
              ) : null}
             <Button 
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo==0} onClick={()=>this.getTakmicari(this.state.pageNo-1)}>
            Previous
          </Button>
          <Button
            style={{ margin: 3, width: 90}}
            disabled={this.state.pageNo==this.state.totalPages-1} onClick={()=>this.getTakmicari(this.state.pageNo+1)}>
            Next
          </Button>
              <Table>
                <thead>
                  <tr>
                    <th>Ime i prezime takmicara</th>
                    <th>Drzava</th>
                    <th>Datum rodjenja</th>
                    <th>Broj osvojenih medalja</th>
                    
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>{this.renderTakmicari()}</tbody>
              </Table>
              <br />
            </div>
          </div>
        ) : null}
      </>
    );
  }
}

export default withNavigation(withParams(Takmicari));
