import React from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import OGAxios from "../../apis/OGAxios";
import { withNavigation } from "../../routeconf";

class AddTakmicar extends React.Component {
  constructor(props) {
    super(props);

    let params = {
      imePrezime: "",
      brojMedalja: "",
      datumRodjenja: "",
      drzavaId: "",
    };
    this.state = { params: params, drzave: [] };
  }

  componentDidMount() {
    this.getDrzave();
  }

  getDrzave() {
    OGAxios.get("/drzave")
      .then((res) => {
        console.log(res);
        this.setState({ drzave: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  onInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    let params = this.state.params;
    params[name] = value;
    console.log(params);
    this.setState({ params: params });
  }

  renderDrzaveInDropDown() {
    return this.state.drzave.map((drzava) => {
      return (
        <option value={drzava.id} key={drzava.id}>
          {drzava.naziv}
        </option>
      );
    });
  }

  createTakmicenje(e) {
    let params = this.state.params;
    let dto = {
      imePrezime: params.imePrezime,
      brojMedalja: params.brojMedalja,
      datumRodjenja: params.datumRodjenja,
      drzavaId: params.drzavaId,
    };
    console.log("dto: " + JSON.stringify(dto));
    try {
      OGAxios.post("/takmicari", dto).then((res) => {
        console.log(res);
        this.props.navigate("/takmicari");
      });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <>
        <div>
          <Form style={{ width: "100%" }}>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Ime i prezime</Form.Label>
                  <Form.Control
                    name="imePrezime"
                    as="input"
                    type="text"
                    placeholder="Unesi ime i prezime"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>

            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Broj medalja</Form.Label>
                  <Form.Control
                    type="number"
                    name="brojMedalja"
                    min="0"
                    step="1"
                    placeholder="Unesi brojMedalja"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Datum rodjenja</Form.Label>
                  <Form.Control
                    type="date"
                    name="datumRodjenja"
                    onChange={(e) => this.onInputChange(e)}
                  ></Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>Drzava takmicara</Form.Label>
                  <Form.Select
                    name="drzavaId"
                    onChange={(e) => this.onInputChange(e)}
                  >
                    <option>Izaberi drzavu</option>
                    {this.renderDrzaveInDropDown()}
                  </Form.Select>
                </Form.Group>
              </Col>
            </Row>
          </Form>
          <br />
          <Row>
            <Col>
              <Button onClick={(e) => this.createTakmicenje(e)}>
                Dodaj takmicara
              </Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default withNavigation(AddTakmicar);
